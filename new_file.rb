# Тут находится программа, выполняющая обработку данных из файла.
# Тест показывает как программа должна работать.
# В этой программе нужно обработать файл данных data_large.txt.

# Ожидания от результата:

# Корректная обработатка файла data_large.txt;
# Проведена оптимизация кода и представлены ее результаты;
# Production-ready код;
# Применены лучшие практики;

require 'json'
require 'byebug'
require 'date'
require 'minitest/autorun'


class CollectionsCounter
  def self.total_count
    @total_count ||= {
      users: 0,
      browsers: 0,
      sessions: 0
    }
  end

  def self.increase_counter(source)
    total_count[source] += 1
  end
end

class UsersCollection
  def self.collection
    @collection ||= {}
  end

  def self.create(fields)
    id = fields[1].to_i
    self.collection[id] = {
      user_id: id,
      full_name: "#{fields[2]} #{fields[3]}",
      sessions_count: 0,
      total_time: 0,
      max_duration: 0,
      browser_names: [],
      used_ie: false,
      always_used_chrome: true,
      dates: []
    }
    CollectionsCounter.increase_counter(:users)
  end

  def self.find(id)
    user = collection[id.to_i]
  end
end

class BrowsersCollection
  def self.collection
    @collection ||= {}
  end

  def self.find_or_create(name)
    if res = collection[name.to_sym]
      res
    else
      CollectionsCounter.increase_counter(:browsers)
      collection[name.to_sym] = name.upcase
    end
  end
end

def parse_session(fields)
  {
    browser_id: fields[3],
    time: fields[4].to_i,
    date: fields[5],
  }
end

def generate_report
  {
    totalUsers: CollectionsCounter.total_count[:users],
    uniqueBrowsersCount: CollectionsCounter.total_count[:browsers],
    totalSessions: CollectionsCounter.total_count[:sessions],
    allBrowsers: BrowsersCollection.collection.values.sort.join(","),
    usersStats: UsersCollection.collection.values.each_with_object({}) do |user, res|
      res[user[:full_name]] = {
        sessionsCount: user[:sessions_count],
        totalTime: "#{user[:total_time]} min.",
        longestSession: "#{user[:max_duration]} min.",
        browsers: user[:browser_names].sort.join(", "),
        usedIE: user[:used_ie],
        alwaysUsedChrome: user[:always_used_chrome],
        dates: user[:dates].sort{ |i, j| j <=> i }
      }
    end
  }
end

def work(file_name='data_large.txt')
  File.open(file_name).each_line do |line|
    cols = line.chomp.split(',')
    
    if cols[0] == 'user'
      UsersCollection.create(cols)
    else
      session = parse_session(cols)
      CollectionsCounter.increase_counter(:sessions)
      browser_name = BrowsersCollection.find_or_create(cols[3])

      user = UsersCollection.find(cols[1])
      user[:sessions_count] += 1
      user[:total_time] += session[:time]
      if user[:max_duration] < session[:time]
        user[:max_duration] = session[:time]
      end
      user[:browser_names] << browser_name
      user[:used_ie] ||= browser_name.match?(/INTERNET EXPLORER/)
      if user[:always_used_chrome]
        user[:always_used_chrome] = browser_name.match?(/CHROME/)
      end
      user[:dates] << session[:date]
    end
  end

  report = generate_report

  File.write('result_new.json', "#{report.to_json}\n")
end  

class TestMe < Minitest::Test
  def setup
    File.write('result_new.json', '')
    File.write('data.txt',
'user,0,Leida,Cira,0
session,0,0,Safari 29,87,2016-10-23
session,0,1,Firefox 12,118,2017-02-27
session,0,2,Internet Explorer 28,31,2017-03-28
session,0,3,Internet Explorer 28,109,2016-09-15
session,0,4,Safari 39,104,2017-09-27
session,0,5,Internet Explorer 35,6,2016-09-01
user,1,Palmer,Katrina,65
session,1,0,Safari 17,12,2016-10-21
session,1,1,Firefox 32,3,2016-12-20
session,1,2,Chrome 6,59,2016-11-11
session,1,3,Internet Explorer 10,28,2017-04-29
session,1,4,Chrome 13,116,2016-12-28
user,2,Gregory,Santos,86
session,2,0,Chrome 35,6,2018-09-21
session,2,1,Safari 49,85,2017-05-22
session,2,2,Firefox 47,17,2018-02-02
session,2,3,Chrome 20,84,2016-11-25
')
  end

  def test_result
    work("data.txt")
    expected_result = '{"totalUsers":3,"uniqueBrowsersCount":14,"totalSessions":15,"allBrowsers":"CHROME 13,CHROME 20,CHROME 35,CHROME 6,FIREFOX 12,FIREFOX 32,FIREFOX 47,INTERNET EXPLORER 10,INTERNET EXPLORER 28,INTERNET EXPLORER 35,SAFARI 17,SAFARI 29,SAFARI 39,SAFARI 49","usersStats":{"Leida Cira":{"sessionsCount":6,"totalTime":"455 min.","longestSession":"118 min.","browsers":"FIREFOX 12, INTERNET EXPLORER 28, INTERNET EXPLORER 28, INTERNET EXPLORER 35, SAFARI 29, SAFARI 39","usedIE":true,"alwaysUsedChrome":false,"dates":["2017-09-27","2017-03-28","2017-02-27","2016-10-23","2016-09-15","2016-09-01"]},"Palmer Katrina":{"sessionsCount":5,"totalTime":"218 min.","longestSession":"116 min.","browsers":"CHROME 13, CHROME 6, FIREFOX 32, INTERNET EXPLORER 10, SAFARI 17","usedIE":true,"alwaysUsedChrome":false,"dates":["2017-04-29","2016-12-28","2016-12-20","2016-11-11","2016-10-21"]},"Gregory Santos":{"sessionsCount":4,"totalTime":"192 min.","longestSession":"85 min.","browsers":"CHROME 20, CHROME 35, FIREFOX 47, SAFARI 49","usedIE":false,"alwaysUsedChrome":false,"dates":["2018-09-21","2018-02-02","2017-05-22","2016-11-25"]}}}' + "\n"
    assert_equal expected_result, File.read('result_new.json')
    
    # work("data_medium.txt")
    # work("data_large.txt")
  end
end